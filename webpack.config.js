const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react-ts");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "gotphoto",
    projectName: "app-navigation",
    webpackConfigEnv,
    argv,
  });

  return merge(defaultConfig, {
    // customizations go here
    externals: ["react", "react-dom", /^@gotphoto\/.+/],
  });
};
