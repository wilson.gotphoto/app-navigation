// @ts-ignore
import { Button } from "@gotphoto/design-system";

export default function Root(props) {
  const handleRoute = (path: string) => (window.location.href = path);

  return (
    <ul className="px-4 py-2 flex">
      <li>
        <Button type="highlight" onClick={() => handleRoute("/settings")}>
          {"Settings"}
        </Button>
      </li>
      <li>
        <Button onClick={() => handleRoute("/jobs")}>{"Jobs"}</Button>
      </li>
    </ul>
  );
}
